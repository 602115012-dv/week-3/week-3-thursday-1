"use strict";

function showAlert(text) {
  console.log(text);
}

function listFormElement() {
  var formElements = document.getElementsByTagName('form');
  console.log(formElements);
}

function copyTextFromNameToLastName() {
  var nameElement = document.getElementById('firstName');
  var surnameElement = document.getElementById('lastName');
  var firstName = nameElement.value;
  console.log(`firstname is ${firstName}`);
  console.log(`now lastname is ${surnameElement.value}`);
  surnameElement.value = firstName;
}

function Reset() {
  let elements = document.getElementsByTagName('form');
  for (let i = 0; i < elements[0].length; i++) {
    if (elements[0][i].getAttribute('type') == 'radio') {
      elements[0][i].checked = false;
    } else {
      elements[0][i].value = null;
    }
  }

  hideSummary();
}

function copyRadioToEmail() {
  let radioElements = document.getElementById('studentStatus')
      .getElementsByTagName('input');
  for (let i=0; i < radioElements.length; i++) {
    if (radioElements[i].checked) {
      document.getElementById('email').value = 
      radioElements[i].value;
    }
  }
}

function showSummary() {
  let summaryElement = document.getElementById('summary');
  summaryElement.removeAttribute('hidden')

  if (summaryElement.innerHTML != "") {
    summaryElement.innerHTML = "";
  }
  
  let name = document.getElementById('firstName').value;
  summaryElement.innerHTML += `<b>First name</b>: ${name} <br />`;
  
  let lastName = document.getElementById('lastName').value;
  summaryElement.innerHTML += `<b>Last name</b>: ${lastName} <br />`;

  let email = document.getElementById('email').value;
  summaryElement.innerHTML += `<b>Email</b>: ${email} <br />`;

  let password = document.getElementById('password').value;
  summaryElement.innerHTML += `<b>Password</b>: ${password} <br />`;

  let studentStatus = getStatus();
  summaryElement.innerHTML += `<b>Student status</b>: ${studentStatus} <br />`;
}

function getStatus() {
  let radioElements = document.getElementById('studentStatus')
      .getElementsByTagName('input');
  for (let i=0; i < radioElements.length; i++) {
    if (radioElements[i].checked) {
      return radioElements[i].value;
    }
  }
}

function hideSummary() {
  let summaryElement = document.getElementById('summary');
  summaryElement.setAttribute('Hidden', 'true');
}

function isEmpty(id) {
  if (document.getElementById(id).value == "") {
    hideSummary();
  }
}